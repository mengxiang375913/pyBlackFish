# -*- coding:UTF-8 -*-
import json


class Event(object):
    def __init__(self, json_dict):
        
        self.name = json_dict.get('name')
        self.id = json_dict.get('id')
        self.active = json_dict.get('active')
        self.data = json_dict.get('data')
        self.bag = json_dict.get('bag')  # 传递包名
        self.time = json_dict.get('time')
        self.bagPath = json_dict.get('bagPath')


def build_event(rev):
    j = None
    try:
        j = json.loads(rev)
        j = json.loads(j.get('text'))
    except:
        return 'warning'
    jstyle = j.get('style') if j != None else None

    #return type("event", (Event,), j)
    return Event(j)
