# -*- coding:UTF-8 -*-
MIDDLE_MAP = {}


def middle(name):
    def fun1(factory):
        MIDDLE_MAP[name] = factory
        
        def fun2(data, url):
            return factory(data, url)
        
        return fun2
    
    return fun1


@middle("int")
def intmiddle(data, url):
    if isinstance(data, list):
        return int(data[url])
    return


@middle("json")
def jsonmiddle(data, url):
    return


@middle("float")
def floatmiddle(data, url):
    return


@middle("list")
def listmiddle(data, url):
    return


@middle("xml")
def xmlmiddle(data, url):
    return


@middle("html")
def htmlmiddle(data, url):
    return


@middle("string")
def strmiddle(data, url):
    return


def build_middle(name):
    return MIDDLE_MAP.get(name)


if __name__ == '__main__':
    fact = MIDDLE_MAP["sql"]("SqlFactory", "", "", "mysql")
    fact.delete(None)
