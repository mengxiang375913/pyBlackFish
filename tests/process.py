import multiprocessing
from multiprocessing import Manager,Value,Queue,Pool,Process
from multiprocessing import queues
import time
i=0

class Test(object):
    ret = 0

def worker_1(q,d,i,interval,test):
    while True:
        i.value+=2
        print("worker_1 i="+str(i.value))
        time.sleep(interval)
        # q.put(i.value)
        # print("Queue print:"+str(q.qsize()))
        print("end worker_1")
        test.ret.put(i.value)
        if(i.value>100):
            break

def worker_2(q,d,i,interval,test):
     while True:
        i.value-=1
        print("worker_2 i="+str(i.value))
        time.sleep(interval)
        q.put(i.value)
        print("Queue print:"+str(q.qsize()))
        print("end worker_2")
        if not test.ret.empty():
            print(test.ret.get())
        if(i.value>100):
            break


def worker_3(q,d,i,interval,test):
     while True:
        i.value+=1
        print("worker_3 i="+str(i.value))
        time.sleep(interval)
        q.put(i.value)
        print("end worker_3")
        print("Queue print:"+str(q.qsize()))
        if(i.value>100):
            break

def f1(arg):
    time.sleep(1)   # 加这句是为了看出5个5个执行的效果。
    print(arg)

def foo(i,arg,arg2):
    arg.put(i)

    print('say hi',i,arg.qsize())


if __name__ == "__main__":
    v=Value('d',0)
    manager = Manager()
    d = manager.dict()
    l = manager.list()
    q = Queue()
    d['1'] = "work1"
    d['2'] = "work2"
    d['3'] = "work3"
    pool = Pool(3)

    #for n in range(10):
    #    pool.apply(func=f1,args=(q,d,i,n,)) 
    li = queues.Queue(20,ctx=multiprocessing)
    li2 = queues.Queue(20,ctx=multiprocessing)

    #for i in range(3):
        #p = Process(target=foo,args=(i,li,li2,))
        # p.daemon = True
        #p.start()
        # p.join()

    t = Test()
    t.ret=Queue()
    p1 = multiprocessing.Process(target = worker_1, args = (q,d,v,2,t))
    p2 = multiprocessing.Process(target = worker_2, args = (q,d,v,3,t))
    p3 = multiprocessing.Process(target = worker_3, args = (q,d,v,4,t))
                                                                                                
    p1.start()
    p2.start()
    p3.start()

    print("The number of CPU is:" + str(multiprocessing.cpu_count()))
    for p in multiprocessing.active_children():
        print("child   p.name:" + p.name + "\tp.id" + str(p.pid))
    
    print("END!!!!!!!!!!!!!!!!!")