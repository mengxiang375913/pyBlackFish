from events.ioevent import IOEvent 
from events.sliderevent import SliderEvent 

def event_test():
    IO_json = '{"id":1,"name":"LED_IO","cmd": { "device": "LED" },"data":"OFF"}';
    i=IOEvent(IO_json)

    Slider_json1 = '{ "id" : 2 , "name" : "DS18b20" ,"cmd" :"read", "data" : { "count" : 0 }}'
    s = SliderEvent(Slider_json1)

    Slider_json2 = '{ "id" : 3 , "name" : "Video" ,"cmd" :"write", "data" : { "count" : 0 }}'
    s = SliderEvent(Slider_json2)

    Timer_json = '{ "id" : 1 , "name" : "DS18b20" , "style" : "read_only" , "cmd" : { "device" : "DS18b20" , "set" : "read" } , "data" : { "count" : 0 }}'

    Talk_json = '{ "id" : 1 , "name" : "DS18b20" , "style" : "read_only" , "cmd" : { "device" : "DS18b20" , "set" : "read" } , "data" : { "count" : 0 }}'

    Player_json = '{ "id" : 1 , "name" : "DS18b20" , "style" : "read_only" , "cmd" : { "device" : "DS18b20" , "set" : "read" } , "data" : { "count" : 0 }}'

    Chart_json = '{ "id" : 1 , "name" : "DS18b20" , "style" : "read_only" , "cmd" : { "device" : "DS18b20" , "set" : "read" } , "data" : { "count" : 0 }}'



