# -*- coding:UTF-8 -*-
import json
import xml
from helpers.modulelisthelper import ModuleListHelper
from helpers.baglisthelper import BagListHelper as BagsHelper

class ConfigHelper(object):
    def __init__(self, root):
        bag_h = BagsHelper()
        bags = root.getElementsByTagName('bag')
        for bag in bags:
            name = bag.getAttribute('name')
            bag_h.add_bag(self.conf_bag(bag), name)
    
    def conf_mod(self, mod):
        from modules import Module
        try:
            m = Module(mod)
            return m
        except Exception as e:
            return None
    
    def conf_bag(self, bag):
        mod_h = ModuleListHelper()
        mods = bag.getElementsByTagName('module')
        for mod in mods:
            style = mod.getAttribute('style')
            rev_mod = self.conf_mod(mod)
            if rev_mod != None:
                mod_h.add_module(rev_mod)
        return mod_h
