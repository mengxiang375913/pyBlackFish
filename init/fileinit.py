# -*- coding:UTF-8 -*-
import hashlib
import time

import json

from init.socketinit import start_url
url_file = start_url + "/api/Connect"

device_key = None
device_name = None

def set_url_file(url):
    global url_file
    url_file = "http://{}/api/Connect".format(url)

def set_devicename():
    global device_name
    m = hashlib.md5()
    
    m.update(str(time.time() * 1000).encode("utf-8"))
    device_name = m.hexdigest()[0:9]

def load_keyfile():
    global device_key
    with open('./files/key.json') as f:
        device_key = json.load(f).get("key")
    return device_key



