# -*- coding:UTF-8 -*-
def On(self, middle, **kwargs):
    print("this is mysql On")


def Off(self, middle, **kwargs):
    print("this is mysql Off")


def Read(self, middle, **kwargs):
    print("this is mysql Read")


def Check(self, middle, **kwargs):
    print("this is mysql Check")


def Gain(self, middle, **kwargs):
    print("this is mysql Gain")


def config(self, **kwargs):
    print("this is mysql init")


mysql_map = {
    "On": On,
    "Off": Off,
    "Read": Read,
    "Check": Check,
    "Gain": Gain,
    "config": config
}
