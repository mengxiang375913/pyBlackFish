# -*- coding:UTF-8 -*-
from customer import blueprint,customer_build

from customer.mysqlcustomer import mysql_map
from customer.apicustomer import api_map
from customer.filecustomer import file_map
from customer.appcustomer import app_map

CUSTOMER_MAP = [
    blueprint("sql", mysql_map),
    blueprint("file", file_map),
    blueprint("api", api_map),
    blueprint("app", app_map),
]

if __name__ == '__main__':
    f = customer_build("sql")
    f.run(f,"On","qq",age=1)
    