# -*- coding:UTF-8 -*-
def On(self, data):
    print("this is file On")


def Off(self, data):
    print("this is file Off")


def Read(self, data):
    print("this is file Read")


def Check(self, data):
    print("this is file Check")


def Gain(self, **kwargs):
    print("this is file Gain")


def config(self, **kwargs):
    print("this is file init")

file_map = {
    "On": On,
    "Off": Off,
    "Read": Read,
    "Check": Check,
    "Gain":Gain,
    "config": config
}
