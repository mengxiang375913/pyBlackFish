# -*- coding:UTF-8 -*-
def On(self, middle, **kwargs):
    event = kwargs.get("event")
    cmd = kwargs.get("cmd")
    path = kwargs.get("path")
    if event!=None and cmd!=None:
        import os
        os.system(path+" "+cmd)
        print("System run:{} {}".format(path,cmd))
    print("this is app On")


def Off(self, middle, **kwargs):
    event = kwargs.get("event")
    cmd = kwargs.get("cmd")
    path = kwargs.get("path")
    if event != None and cmd != None:
        import os
        os.system(path + " " + cmd)
        print("System run:{} {}".format(path, cmd))
    print("this is app Off")


def Read(self, middle, **kwargs):
    print("this is app Read")


def Check(self, middle, **kwargs):
    print("this is app Check")


def Gain(self, middle, **kwargs):
    print("this is app Gain")


def config(self, **kwargs):
    print("this is app init")

app_map = {
    "On": On,
    "Off": Off,
    "Read": Read,
    "Check": Check,
    "Gain":Gain,
    "config": config
}
