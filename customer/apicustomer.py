# -*- coding:UTF-8 -*-
def On(self, middle, **kwargs):
    print("this is api On")


def Off(self, middle, **kwargs):
    print("this is api Off")


def Read(self, middle, **kwargs):
    print("this is api Read")


def Check(self, middle, **kwargs):
    print("this is api Check")


def Gain(self, middle, **kwargs):
    print("this is api Gain")


def config(self, **kwargs):
    print("this is api init")

api_map = {
    "On": On,
    "Off": Off,
    "Read": Read,
    "Check": Check,
    "Gain":Gain,
    "config": config
}
