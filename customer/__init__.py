# from core.factory import build
from .appcustomer import app_map
from .apicustomer import api_map
from .mysqlcustomer import mysql_map
from .filecustomer import file_map

CUSTOMER_MAP = {
    "app": app_map,
    "file": file_map,
    "api": api_map,
    "sql": mysql_map
}


def blueprint(url, sql_dict):
    CUSTOMER_MAP[url] = sql_dict


class Customer(object):
    def __init__(self,map,config):
        self.map = map
        self.config = config

    def run(self, name, middle, **kwargs):
        f = self.map.get(name)
        if f is None:
            return None
        try:
            f(self, middle, **kwargs)
        except Exception as e:
            print("Customer Error:{}".format(e))


def customer_build(name):
    if CUSTOMER_MAP.get(name) == None:
        return None
    f = Customer(CUSTOMER_MAP[name],CUSTOMER_MAP[name].get("config")) #type(name, (object,), {"map": CUSTOMER_MAP[name], "run": run, "config": CUSTOMER_MAP[name].get("config")})
    return f
