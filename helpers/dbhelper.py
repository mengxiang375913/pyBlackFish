# -*- coding:UTF-8 -*-
import json
import logging


class DBConfiger(object):
    __db_name = None
    __db_pass = None
    __db_path = None
    __db_port = None
    
    def __init__(self):
        self.db_conf_data = None
        try:
            self.db_conf_data = json.load("/files/db.json")
        except Exception as e:
            logging.error("")
        
        if self.db_conf_data is None:
            return
        try:
            self.__db_name = self.db_conf_data["db_name"]
        except Exception as e:
            logging.error("")


class DBHelper:
    def __new__(cls):
        pass
    
    def __init__(self):
        pass


class RedisHelper(object):
    def __init__(self,db_config):
        pass


class MysqlHelper(object):
    def __init__(self,db_config):
        pass


class SqliteHelper(object):
    def __init__(self,db_config):
        pass
