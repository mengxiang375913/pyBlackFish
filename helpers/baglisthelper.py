# -*- coding:UTF-8 -*-
# 将多个模块打包，name作为bag 的引用，id作为bag下module的引用
# mod_helper 等价于 bag
from helpers.modulelisthelper import ModuleListHelper as Bag

bag_mod = None

class BagListHelper(object):
    """description of class"""
    _instance = {}
    bag_map = {}
    def __new__(cls, *args, **kwargs):
        global bag_mod
        if bag_mod is None:
            bag_mod = super().__new__(cls,*args, **kwargs)
        return bag_mod
        
    
    def add_bag(self, mod_h, name):  # 不支持动态加载
        # 解析 <bag-name>
        # name = x['name']
        self.bag_map[name] = mod_h
    
    def del_bag(self, name):
        try:
            del self.bag_map[str(name)]
        except:
            return 'warning'
        return 'ok'
    
    def find_bag(self, name):
        try:
            return self.bag_map[str(name)]
        except:
            return None
    
    def update_bag(self, name, mod_h):
        try:
            self.bag_map[name] = mod_h
        except:
            return 'warning'
    
    # 添加mod过程
    # 使用包名，找到mod_helper,在mod_helper中使用id添加
